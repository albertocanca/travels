package com.goncanapp.travels;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Alberto on 11/09/2015.
 */
public class Gastos extends AppCompatActivity {

    Context contexto;
    public static ViajesBD viajesBD;
    String nombre, nombreViaje;
    FloatingActionButton btnMas;
    public static CollapsingToolbarLayout ctlLayout;
    RecyclerView lstLista;
    Intent i;
    ArrayList<Compra> datos;
    public static int id_viaje, idSeleccionadoGasto, posSeleccionado;
    static GastosAdaptador adaptador;
    double importeTotal = 0;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gastos);
        contexto = this;

        //Appbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.appbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        //Agragar titulo en ActionBar
        TextView titulo = (TextView) findViewById(R.id.titulo);
        titulo.setText(R.string.app_name);

        Bundle extras = getIntent().getExtras();
        id_viaje = extras.getInt("id_viaje");
        Boolean gastoBorrado = extras.getBoolean("gastoBorrado");
        if(gastoBorrado){
            //Snackbar.make(, "Esto es una prueba", Snackbar.LENGTH_LONG)
              //      .show();
        }
        viajesBD = new ViajesBD(contexto);

        //Agregar titulo en ActionBar
        lstLista = (RecyclerView)findViewById(R.id.lstLista);

        //Leer Preferencias
        SharedPreferences pref = getSharedPreferences("com.goncanapp.travels_preferences", Context.MODE_PRIVATE);
        int ordenacion = Integer.parseInt(pref.getString("ordenacionGastos", "1"));

        //rellenarListaGastos();
        datos = new ArrayList<>();
        Cursor cursor  = viajesBD.obtenerGastos(id_viaje, ordenacion);
        if(cursor.getCount() > 0){
            LinearLayout layout = (LinearLayout) findViewById(R.id.noGasto);
            layout.setVisibility(View.INVISIBLE);
        }
        while (cursor.moveToNext()) {
            String aux = cursor.getString(1);
            datos.add(new Compra(aux, cursor.getString(3), cursor.getInt(0)));
            importeTotal += Double.parseDouble(cursor.getString(2));
        }

        adaptador = new GastosAdaptador(datos);
        lstLista.setAdapter(adaptador);

        lstLista.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        lstLista.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL_LIST));
        lstLista.setItemAnimator(new DefaultItemAnimator());

        ctlLayout = (CollapsingToolbarLayout)findViewById(R.id.ctlLayout);
        colocarNombre(importeTotal, false);

        //Boton añadir gasto
        i = new Intent(this, AnadirGasto.class);
        btnMas = (FloatingActionButton)findViewById(R.id.btnFab);
        btnMas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                i.putExtra("id_viaje", id_viaje);
                startActivity(i);
            }
        });

        //Seleccionar Item
        lstLista.addOnItemTouchListener(new RecyclerItemClickListener(contexto, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        idSeleccionadoGasto = datos.get(position).getId();
                        posSeleccionado = position;
                        MainFragment fragment = new MainFragment();
                        getSupportFragmentManager()
                                .beginTransaction()
                                .add(android.R.id.content, fragment, "MainFragment")
                                .commit();
                        new SimpleListDialog().show(fragment.getFragmentManager(), "SimpleListDialog");

                    }
                })
        );
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_gastos, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.configuracion) {
            Intent i = new Intent(this, Preferencias.class);
            startActivity(i);
            return true;
        }else if (id == R.id.anadir_gasto) {
            Intent i = new Intent(this, AnadirGasto.class);
            i.putExtra("id_viaje", id_viaje);
            startActivity(i);
            return true;
        }else if (id == R.id.verViaje) {
            Intent i = new Intent(this, Viaje.class);
            i.putExtra("id_viaje", id_viaje);
            startActivity(i);
            return true;
        }else if (id == R.id.anadir_Viaje) {
            Intent i = new Intent(this, AnadirViaje.class);
            i.putExtra("modificar", false);
            startActivity(i);
            return true;
        }else if (id == R.id.viajes) {
            Intent i = new Intent(this, MainActivity.class);
            startActivity(i);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    public void onBackPressed() {
        Intent i = new Intent(this, Viaje.class);
        i.putExtra("id_viaje", id_viaje);
        startActivity(i);
    }

    public void colocarNombre(double resta, boolean restar){
        ctlLayout = (CollapsingToolbarLayout)findViewById(R.id.ctlLayout);
        Cursor cursorViaje = viajesBD.obtenerViaje(id_viaje);
        cursorViaje.moveToNext();
        nombreViaje = cursorViaje.getString(1);
        String tipo = cursorViaje.getString(3);
        if(restar){
            importeTotal -=resta;
        }
        if(importeTotal>0){
            nombre = nombreViaje + " - " + String.format("%.2f", importeTotal) + " €";
        }else{
            nombre = nombreViaje;
        }
        ctlLayout.setTitle(nombre);

        //Setear imagen
        ImageView image = (ImageView) findViewById(R.id.imgToolbar);
        if(tipo.equals("Vacaciones")){
            image.setImageResource(R.drawable.vacaciones);
        }else if(tipo.equals("Senderismo")) {
            image.setImageResource(R.drawable.senderismo);
        }else if(tipo.equals("Cultural")) {
            image.setImageResource(R.drawable.cultural);
        }else if(tipo.equals("Trabajo")) {
            image.setImageResource(R.drawable.trabajo);
        }else if(tipo.equals("Gastronómico")) {
            image.setImageResource(R.drawable.gastronomia);
        }

    }
}
