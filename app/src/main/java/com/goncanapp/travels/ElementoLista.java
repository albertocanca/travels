package com.goncanapp.travels;

/**
 * Created by Alberto on 09/09/2015.
 */
public class ElementoLista {

    private int imagen;
    private String titulo;
    private int iD;

    public ElementoLista() {
        super();
    }

    public ElementoLista(int imagen, String titulo, int id) {
        super();
        this.imagen = imagen;
        this.titulo = titulo;
        this.iD = id;
    }

    public int getImagen() {
        return imagen;
    }
    public void setImagen(int imagen) {
        this.imagen = imagen;
    }
    public String getTitulo() {
        return titulo;
    }
    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }
    public int getID() {
        return iD;
    }
    public void setTitulo(int id ) {
        this.iD = id;
    }
}