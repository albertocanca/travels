package com.goncanapp.travels;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Alberto on 15/11/2015.
 */
public class Util {

    public Date convertStringToDate (String startDateString){
        DateFormat df = new SimpleDateFormat("MM-dd-yyyy");
        Date startDate = new Date();
        try {
            startDate = df.parse(startDateString);
            String newDateString = df.format(startDate);
            System.out.println(newDateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return startDate;
    }

    public Date ConvertToDate(String dateString){
        String aux = dateString.replace("-","/");
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date convertedDate = new Date();
        try {
            convertedDate = dateFormat.parse(aux);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return convertedDate;
    }

    public Date getFormatDate(){
        String aux = getDate().replace("-","/");
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date convertedDate = new Date();
        try {
            convertedDate = dateFormat.parse(aux);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return convertedDate;
    }

    public String getDateTime() {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date date = new Date();
        return dateFormat.format(date);
    }

    public String getDate() {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date date = new Date();
        return dateFormat.format(date);
    }
}
