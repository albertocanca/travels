package com.goncanapp.travels;

import android.os.Bundle;
import android.preference.PreferenceActivity;

/**
 * Created by Alberto on 27/09/2015.
 */
public class Preferencias extends PreferenceActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);

    }
}
