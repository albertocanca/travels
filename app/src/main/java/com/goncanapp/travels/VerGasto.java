package com.goncanapp.travels;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by Alberto on 24/09/2015.
 */
public class VerGasto extends AppCompatActivity {

    Gastos gastos;
    GastosAdaptador gastosAdaptador;
    ViajesBD viajesBD;
    Context contexto;
    int id_gasto, id_viaje;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ver_gasto);
        contexto = this;
        gastosAdaptador = Gastos.adaptador;
        gastos = new Gastos();

        viajesBD = new ViajesBD(contexto);

        Bundle extras = getIntent().getExtras();
        id_gasto = extras.getInt("id_gasto");
        Cursor cursor = viajesBD.obtenerGasto(id_gasto);
        cursor.moveToNext();
        id_viaje = Integer.parseInt(cursor.getString(5));

        TextView nombreGasto = (TextView) findViewById(R.id.verNombreGasto);
        nombreGasto.setText(cursor.getString(1));
        TextView importe = (TextView) findViewById(R.id.verImportegasto);
        importe.setText(cursor.getString(2) + " €");
        TextView tipoGasto = (TextView) findViewById(R.id.verTipoGasto);
        tipoGasto.setText(cursor.getString(3));
        TextView fechaGasto = (TextView) findViewById(R.id.verFechaGasto);
        fechaGasto.setText(cursor.getString(4));

        //Setear imagen
        ImageView image = (ImageView) findViewById(R.id.imgToolbar);
        Log.i("Aplicacion", "1:" + tipoGasto);
        if(cursor.getString(3).equals("Restaurante/Bar")){
            image.setImageResource(R.drawable.bar);
        }else if(cursor.getString(3).equals("Transporte")) {
            image.setImageResource(R.drawable.transporte);
        }else if(cursor.getString(3).equals("Alojamiento")) {
            image.setImageResource(R.drawable.alojamiento);
        }else if(cursor.getString(3).equals("Ticket/Entrada")) {
            image.setImageResource(R.drawable.ticket);
        }else if(cursor.getString(3).equals("Comida/Bebida")) {
            image.setImageResource(R.drawable.comida);
        }



        //Appbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.appbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        //Agragar titulo en ActionBar
        TextView titulo = (TextView) findViewById(R.id.titulo);
        Cursor cursorViaje = viajesBD.obtenerViaje(id_viaje);
        cursorViaje.moveToNext();
        titulo.setText("Gasto del viaje " + cursorViaje.getString(1));

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_anadir_gastos, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == R.id.configuracion) {
            Intent i = new Intent(this, Preferencias.class);
            startActivity(i);
            return true;
        }
        if (id == R.id.verViaje) {
            Intent i = new Intent(this, Viaje.class);
            i.putExtra("id_viaje", id_viaje);
            startActivity(i);
            return true;
        }
        if (id == R.id.anadir_Viaje) {
            Intent i = new Intent(this, AnadirViaje.class);
            i.putExtra("modificar", false);
            startActivityForResult(i, 1);
            return true;
        }else if (id == R.id.viajes) {
            Intent i = new Intent(this, MainActivity.class);
            startActivity(i);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void eliminarGasto(View view){
        viajesBD.eliminarGasto(id_gasto);
        gastosAdaptador.lanzarEliminarGastoArray(Gastos.posSeleccionado);
        Intent i = new Intent(this, Gastos.class);
        i.putExtra("gastoBorrado", true);
        i.putExtra("id_viaje", id_viaje);
        startActivity(i);
    }
}
