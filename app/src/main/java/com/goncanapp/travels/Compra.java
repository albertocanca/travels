package com.goncanapp.travels;

public class Compra
{
    private String titulo;
    private String subtitulo;
    private int id_compra;

    public Compra(String tit, String sub, int id){
        titulo = tit;
        subtitulo = sub;
        id_compra =id;
    }

    public String getTitulo(){
        return titulo;
    }

    public String getSubtitulo(){
        return subtitulo;
    }

    public int getId(){ return id_compra;}
}
