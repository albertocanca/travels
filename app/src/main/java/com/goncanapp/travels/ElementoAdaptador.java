package com.goncanapp.travels;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Alberto on 09/09/2015.
 */
public class ElementoAdaptador extends BaseAdapter {

    private Context context;
    private List<ElementoLista> items;

    public ElementoAdaptador(Context context, List<ElementoLista> items) {

        this.context = context;
        this.items = items;
    }

    @Override
    public int getCount() {
        return this.items.size();
    }

    @Override
    public Object getItem(int position) {
        return this.items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View rowView = convertView;

        if (convertView == null) {
            // Create a new view into the list.
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = inflater.inflate(R.layout.lista, parent, false);
        }

        // Set data into the view.
        ImageView ivItem = (ImageView) rowView.findViewById(R.id.imagenViaje);
        TextView tvTitle = (TextView) rowView.findViewById(R.id.nombreViajeLista);

        ElementoLista item = this.items.get(position);
        tvTitle.setText(item.getTitulo());
        ivItem.setImageResource(item.getImagen());

        return rowView;
    }

}