package com.goncanapp.travels;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Alberto on 07/09/2015.
 */
public class ViajesBD extends SQLiteOpenHelper {

    Util util;

    public ViajesBD (Context context){
        super(context, "Viajes", null, 2);
        util = new Util();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //Crear tabla VIAJES
        db.execSQL("CREATE TABLE viajes( viaje_id INTEGER PRIMARY KEY AUTOINCREMENT, nombre TEXT, lugar Text, tipo TEXT, fechaInicio TEXT, fechaFin TEXT, fechaCreacion TEXT)");
        db.execSQL("INSERT INTO viajes VALUES (null, 'Mi primer viaje', 'Sevilla', 'Cultural', '" + util.getDate()+"' , '"+ util.getDate()+"', '"+ util.getDateTime()+"')");
        db.execSQL("INSERT INTO viajes VALUES (null, 'Viaje Cádiz', 'Sevilla', 'Vacaciones', '" + util.getDate()+"' , '"+ util.getDate()+"', '"+ util.getDateTime()+"')");

        //Crear tabla GASTOS
        db.execSQL("CREATE TABLE gastos( gasto_id INTEGER PRIMARY KEY AUTOINCREMENT, nombre TEXT, importe Text, tipo TEXT, fecha TEXT, viaje INTEGER, fechaCreacion TEXT, " +
                "FOREIGN KEY (viaje) REFERENCES viajes (viaje_id))");
        db.execSQL("INSERT INTO gastos VALUES (null, 'Autobús', '3.70', 'Ticket/Entrada', '"+ util.getDate()+"', 1, '"+ util.getDateTime()+"')");
        db.execSQL("INSERT INTO gastos VALUES (null, 'Alojamiento', '10.50', 'Ticket/Entrada', '"+ util.getDate()+"', 1, '"+ util.getDateTime()+"')");
        db.execSQL("INSERT INTO gastos VALUES (null, 'Tren', '1.70', 'Ticket/Entrada', '"+ util.getDate()+"', 1, '"+ util.getDateTime()+"')");
        db.execSQL("INSERT INTO gastos VALUES (null, 'Cena', '15.90', 'Restaurante/Bar', '"+ util.getDate()+"', 1, '"+ util.getDateTime()+"')");
        db.execSQL("INSERT INTO gastos VALUES (null, 'Museo', '9', 'Ticket/Entrada', '"+ util.getDate()+"', 1, '"+ util.getDateTime()+"')");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public void guardarViaje(String nombre, String lugar, String  tipo, String FechaI, String FechaF) {
        SQLiteDatabase db = getWritableDatabase();
        String fechaActual = util.getDateTime();
        db.execSQL("INSERT INTO viajes VALUES (null, '" + nombre + "', '" + lugar + "' , '" + tipo + "' , '" + FechaI + "' , '" + FechaF + "' , '" + fechaActual + "')");
        db.close();
    }

    public void modificarViaje(String nombre, String lugar, String  tipo, String fechaI, String fechaF, int id) {
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("UPDATE viajes SET nombre = '" + nombre +
                "' , lugar = '" + lugar +
                "' , tipo = '" + tipo +
                "' , fechaInicio = '" + fechaI +
                "' , fechaFin = '" + fechaF +
                "' WHERE viaje_id = " + id);
        db.close();
    }

    public Cursor obtenerViajes(int ordenacion){
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM viajes ORDER BY nombre ASC", null);
        if(ordenacion == 1){
            cursor = db.rawQuery("SELECT * FROM viajes ORDER BY fechaCreacion ASC", null);
        }else if(ordenacion == 2){
            cursor = db.rawQuery("SELECT * FROM viajes ORDER BY fechaInicio ASC", null);
        }else if (ordenacion == 3){
            cursor = db.rawQuery("SELECT * FROM viajes ORDER BY UPPER(nombre) ASC", null);
        }
        return cursor;
    }

    public Cursor obtenerViaje(int id){
        SQLiteDatabase db = getReadableDatabase();
        return db.rawQuery("SELECT * FROM viajes WHERE viaje_id = " +id, null);
    }

    public void eliminarViaje(int id){
        SQLiteDatabase db = getReadableDatabase();
        db.execSQL("DELETE FROM viajes WHERE viaje_id = " +id);
        db.close();
    }

    public Cursor obtenerGastos(int id, int ordenacion){
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM gastos WHERE viaje = " + id, null);
        if(ordenacion == 1){
            cursor = db.rawQuery("SELECT * FROM gastos WHERE viaje = " + id + " ORDER BY fecha ASC", null);
        }else if(ordenacion == 2){
            cursor = db.rawQuery("SELECT * FROM gastos WHERE viaje = " + id + " ORDER BY UPPER(nombre) ASC", null);
        }else if (ordenacion == 3){
            cursor = db.rawQuery("SELECT * FROM gastos WHERE viaje = " + id + " ORDER BY importe ASC", null);
        }
        return cursor;
    }

    public Cursor obtenerGasto(int id){
        SQLiteDatabase db = getReadableDatabase();
        return db.rawQuery("SELECT * FROM gastos WHERE gasto_id = " +id, null);
    }

    public void guardarGasto(String nombre, String importe, String  tipo, String FechaI, int viaje) {
        SQLiteDatabase db = getWritableDatabase();
        String fechaActual = util.getDateTime();
        db.execSQL("INSERT INTO gastos VALUES (null, '" + nombre + "', '" + importe + "' , '"  + tipo + "' , '"  + FechaI + "' , '" + viaje + "' , '" + fechaActual + "')");
        db.close();
    }

    public void modificarGasto(String nombre, String importe, String  tipo, String fechaI, int viaje, int id) {
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("UPDATE gastos SET nombre = '"+ nombre +
                "' , importe = '" + importe +
                "' , tipo = '" + tipo +
                "' , fecha = '" + fechaI +
                "' , viaje = '" + viaje +
                "' WHERE gasto_id = " + id);
        db.close();
    }

    public void eliminarGasto(int id){
        SQLiteDatabase db = getReadableDatabase();
        db.execSQL("DELETE FROM gastos WHERE gasto_id = " +id);
        db.close();
    }


}
