package com.goncanapp.travels;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Alberto on 13/09/2015.
 */
public class GastosAdaptador extends RecyclerView.Adapter<GastosAdaptador.TitularesViewHolder> implements View.OnClickListener {

    Compra item;
    private View.OnClickListener listener;
    private ArrayList<Compra> datos;

    public static class TitularesViewHolder extends RecyclerView.ViewHolder {

        private TextView txtTitulo;
        private TextView txtSubtitulo;

        public TitularesViewHolder(View itemView) {
            super(itemView);

            txtTitulo = (TextView)itemView.findViewById(R.id.tituloGasto);
            txtSubtitulo = (TextView)itemView.findViewById(R.id.subtituloGasto);
        }

        public void bindTitular(Compra c) {
            txtTitulo.setText(c.getTitulo());
            txtSubtitulo.setText(c.getSubtitulo());
        }
    }

    public GastosAdaptador(ArrayList<Compra> datos) {
        this.datos = datos;
    }

    @Override
    public TitularesViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        final View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.lista_elemento, viewGroup, false);

        itemView.setOnClickListener(this);
        //android:background="?android:attr/selectableItemBackground"

        return new TitularesViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(TitularesViewHolder viewHolder, int pos) {
        item = datos.get(pos);
        viewHolder.bindTitular(item);

    }

    @Override
    public int getItemCount() {
        return datos.size();
    }

    public void setOnClickListener(View.OnClickListener listener) {
        this.listener = listener;
    }

    @Override
    public void onClick(View view) {
        if(listener != null)
            listener.onClick(view);
    }

    public void lanzarEliminarGastoArray (int idGasto){
        datos.remove(idGasto);
        notifyDataSetChanged();
    }


}
