package com.goncanapp.travels;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by Alberto on 10/09/2015.
 */
public class Viaje extends AppCompatActivity {

    private Context contexto;
    ViajesBD  viajesBD;
    String nombre, fechaI, fechaF, tipo, lugar;
    int id_viaje;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.viaje);

        //Appbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.appbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        //Obtener valores del viaje de la base de datos
        Bundle extras = getIntent().getExtras();
        id_viaje = extras.getInt("id_viaje");
        contexto = this;
        viajesBD = new ViajesBD(contexto);
        Cursor cursor = viajesBD.obtenerViaje(id_viaje);
        cursor.moveToNext();
        nombre = cursor.getString(1);
        lugar = cursor.getString(2);
        tipo = cursor.getString(3);
        fechaI = cursor.getString(4);
        fechaF = cursor.getString(5);


        //Agragar titulo en ActionBar
        TextView titulo = (TextView) findViewById(R.id.titulo);
        titulo.setText(nombre);

        //Setear imagen
        ImageView image = (ImageView) findViewById(R.id.imgToolbar);
        image.setImageResource(R.drawable.cesped);
        if(tipo.equals("Vacaciones")){
            image.setImageResource(R.drawable.vacaciones);
        }else if(tipo.equals("Senderismo")) {
            image.setImageResource(R.drawable.senderismo);
        }else if(tipo.equals("Cultural")) {
            image.setImageResource(R.drawable.cultural);
        }else if(tipo.equals("Trabajo")) {
            image.setImageResource(R.drawable.trabajo);
        }else if(tipo.equals("Gastronómico")) {
            image.setImageResource(R.drawable.gastronomia);
        }

        /*int newHeight = getWindowManager().getDefaultDisplay().getHeight() / 4;
        int orgWidth = image.getDrawable().getIntrinsicWidth();
        int orgHeight = image.getDrawable().getIntrinsicHeight();

        int newWidth = (int) Math.floor((orgWidth * newHeight) / orgHeight);

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(newWidth, newHeight);
        params.setMargins(20, 20, 20, 10);
        image.setLayoutParams(params);
        image.setScaleType(ImageView.ScaleType.FIT_XY);*/

        //Setear nombre del Viaje
        TextView nombreT = (TextView) findViewById(R.id.verNombreViaje);
        nombreT.setText(nombre);
        TextView lugarT = (TextView) findViewById(R.id.verLugar);
        lugarT.setText(lugar);
        TextView tipoT = (TextView) findViewById(R.id.verTipoViaje);
        tipoT.setText(tipo);
        TextView fechaIT = (TextView) findViewById(R.id.verFechaViajeInicio);
        fechaIT.setText(fechaI);
        TextView fechaFT = (TextView) findViewById(R.id.verFechaViajeFin);
        fechaFT.setText(fechaF);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_viaje, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.anadir_Viaje) {
            Intent i = new Intent(this, AnadirViaje.class);
            i.putExtra("modificar", false);
            startActivityForResult(i, 1);
            return true;
        }
        if (id == R.id.anadir_gasto) {
            Intent i = new Intent(this, AnadirGasto.class);
            i.putExtra("id_viaje", id_viaje);
            startActivity(i);
            return true;
        }
        if (id == R.id.viajes) {
            Intent i = new Intent(this, MainActivity.class);
            startActivity(i);
            return true;
        }
        if (id == R.id.configuracion) {
            Intent i= new Intent(this,Preferencias.class);
            startActivityForResult(i, 1);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void eliminarViaje(View view){
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Eliminar viaje")
                .setMessage("¿Estás seguro de eliminar este viaje?")
                .setNegativeButton(android.R.string.cancel, null)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        viajesBD.eliminarViaje(id_viaje);
                        Intent i = new Intent(Viaje.this, MainActivity.class);
                        startActivity(i);
                    }
                }).show();

    }

    public void verGastosViaje(View view){
        Intent i = new Intent(this, Gastos.class);
        i.putExtra("id_viaje", id_viaje);
        startActivity(i);
    }

    public void modificarViaje(View view){
        Intent i = new Intent(this, AnadirViaje.class);
        i.putExtra("modificar", true);
        i.putExtra("id_viaje", id_viaje);
        startActivity(i);
    }

    public void onBackPressed(){
        Intent i = new Intent(this, MainActivity.class);
        startActivity(i);
    }
}
