package com.goncanapp.travels;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

/**
 * Fragmento con diálogo de lista simple
 */
public class SimpleListDialog extends DialogFragment {

    Gastos gastos;
    ViajesBD viajesBD;
    GastosAdaptador gastosAdaptador;

    public SimpleListDialog() {
        gastos = new Gastos();
        viajesBD = Gastos.viajesBD;
        gastosAdaptador = Gastos.adaptador;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return createSingleListDialog();
    }

    /**
     * Crea un Diálogo con una lista de selección simple
     *
     * @return La instancia del diálogo
     */
    public AlertDialog createSingleListDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        final CharSequence[] items = new CharSequence[2];

        items[0] = "Ver";
        items[1] = "Modificar";

        builder.setTitle("¿Que quieres hacer con este gasto?")
            .setItems(items, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (which == 0){
                        Intent i = new Intent(getActivity(), VerGasto.class);
                        i.putExtra("id_gasto", Gastos.idSeleccionadoGasto);
                        startActivity(i);
                    }else if (which == 1){
                        Intent i = new Intent(getActivity(), AnadirGasto.class);
                        i.putExtra("modificar", true);
                        i.putExtra("id_viaje", Gastos.id_viaje);
                        i.putExtra("id_gasto", Gastos.idSeleccionadoGasto);
                        startActivity(i);
                    }
                }
            });
        builder.setIcon(R.drawable.ic_fab_icon);

        return builder.create();
    }


}

