package com.goncanapp.travels;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.view.View.OnClickListener;
import android.widget.Toast;

import java.util.Calendar;
import java.util.Date;


/**
 * Created by Alberto on 06/09/2015.
 */
public class AnadirViaje extends AppCompatActivity implements OnClickListener{

    ImageButton btnFechaInicio, btnFechaFin, btnMasLugar;
    EditText textFechaInicio, textFechaFin, nombreViaje, lugarViaje;
    private int mYear, mMonth, mDay;
    Button botonNuevoViaje;
    private Context contexto;
    Spinner tipo;
    int id_viaje;
    boolean modificar;
    Util util;


    private static ViajesBD viajesBD;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.anadir_viaje);
        contexto = this;
        util =  new Util();

        //Appbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.appbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        //Agragar titulo en ActionBar
        TextView titulo = (TextView) findViewById(R.id.titulo);
        titulo.setText(R.string.anadir_viaje);

        //Fecha
        textFechaInicio = (EditText) findViewById(R.id.fechaInicio);
        textFechaFin = (EditText) findViewById(R.id.fechaFin);
        btnFechaInicio = (ImageButton) findViewById(R.id.botonFechaInicio);
        btnFechaInicio.setOnClickListener(this);
        btnFechaFin = (ImageButton) findViewById(R.id.botonFechaFin);
        btnFechaFin.setOnClickListener(this);

        nombreViaje = (EditText) findViewById(R.id.nombreViaje);
        lugarViaje = (EditText) findViewById(R.id.lugar);
        tipo =(Spinner) findViewById(R.id.tipoViaje);

        botonNuevoViaje = (Button) findViewById(R.id.aceptarViaje);

        viajesBD = new ViajesBD(contexto);

        //Obtener valores del viaje de la base de datos
        Bundle extras = getIntent().getExtras();
        modificar = extras.getBoolean("modificar");
        if(modificar){
            id_viaje = extras.getInt("id_viaje");
            contexto = this;
            viajesBD = new ViajesBD(contexto);
            Cursor cursor = viajesBD.obtenerViaje(id_viaje);
            cursor.moveToNext();
            nombreViaje.setText(cursor.getString(1));
            lugarViaje.setText(cursor.getString(2));
            textFechaInicio.setText(cursor.getString(4));
            textFechaFin.setText(cursor.getString(5));

            String[] tipos = getResources().getStringArray(R.array.tipoViaje);
            for(int i=0; i<tipos.length; i++){
                if(tipos[i].equals(cursor.getString(3))){
                    tipo.setSelection(i);
                }
            }

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_anadir_viaje, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.viajes) {
            Intent i = new Intent(this, MainActivity.class);
            startActivity(i);
            return true;
        }else if (id == R.id.configuracion) {
            Intent i= new Intent(this,Preferencias.class);
            startActivityForResult(i, 1);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.botonFechaInicio:
                // Process to get Current Date
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dpd = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                // Setear valor en editText
                                textFechaInicio.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                            }
                        }, mYear, mMonth, mDay);
                dpd.show();
                break;
            case R.id.botonFechaFin:
                // Process to get Current Date
                final Calendar cFin = Calendar.getInstance();
                mYear = cFin.get(Calendar.YEAR);
                mMonth = cFin.get(Calendar.MONTH);
                mDay = cFin.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dpdFin = new DatePickerDialog(this,new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                // Setear valor en editText
                                textFechaFin.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                            }
                        }, mYear, mMonth, mDay);
                dpdFin.show();
                break;
        }
    }

    public void aceptarViaje(View view) {
        Date dateInicio = util.convertStringToDate(textFechaInicio.getText().toString());
        Date dateFin = util.convertStringToDate(textFechaFin.getText().toString());
        if (nombreViaje.getText().toString().matches("") || lugarViaje.getText().toString().matches("") || tipo.getSelectedItem().toString().matches("") ||
                textFechaInicio.getText().toString().matches("") || textFechaFin.getText().toString().matches("")) {
            Toast toast = new Toast(getApplicationContext());

            LayoutInflater inflater = getLayoutInflater();
            View layout = inflater.inflate(R.layout.toast_layout, (ViewGroup) findViewById(R.id.lytLayout));

            TextView txtMsg = (TextView) layout.findViewById(R.id.txtMensaje);
            txtMsg.setText(R.string.errorForm);

            toast.setDuration(Toast.LENGTH_SHORT);
            toast.setView(layout);
            toast.show();
        }
        if(dateInicio.compareTo(dateFin) > 0){
            Toast toast = new Toast(getApplicationContext());

            LayoutInflater inflater = getLayoutInflater();
            View layout = inflater.inflate(R.layout.toast_layout, (ViewGroup) findViewById(R.id.lytLayout));

            TextView txtMsg = (TextView)layout.findViewById(R.id.txtMensaje);
            txtMsg.setText(R.string.errorFormDates);

            toast.setDuration(Toast.LENGTH_SHORT);
            toast.setView(layout);
            toast.show();
        }else{
            if(modificar){
                viajesBD.modificarViaje(nombreViaje.getText().toString(), lugarViaje.getText().toString(), tipo.getSelectedItem().toString(),
                        textFechaInicio.getText().toString(), textFechaFin.getText().toString(), id_viaje);
                Intent i = new Intent(this, MainActivity.class);
                startActivity(i);
                finish();
            }else{
                viajesBD.guardarViaje(nombreViaje.getText().toString(), lugarViaje.getText().toString(), tipo.getSelectedItem().toString(),
                        textFechaInicio.getText().toString(), textFechaFin.getText().toString());
                Intent i = new Intent(this, MainActivity.class);
                startActivity(i);
                finish();
            }

        }
    }

    public void onBackPressed(){
        Intent i = new Intent(this, MainActivity.class);
        startActivity(i);
    }
}
