package com.goncanapp.travels;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import java.util.ArrayList;
import java.util.List;



public class MainActivity extends AppCompatActivity{

    private Context contexto;
    ListView listView;
    ViajesBD  viajesBD;
    List<ElementoLista> items;
    int ordenacion;
    boolean viajesPasados;
    Util util;

    //Search
    private Toolbar toolbar;
    private MenuItem mSearchAction;
    private boolean isSearchOpened = false;
    private EditText edtSeach;
    android.support.v7.app.ActionBar action;

    int i =0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        contexto = this;
        viajesBD = new ViajesBD(contexto);
        util = new Util();

        //Appbar
        toolbar = (Toolbar) findViewById(R.id.appbar);
        setSupportActionBar(toolbar);
        //getSupportActionBar().setDisplayShowTitleEnabled(false);

        //Leer Preferencias
        SharedPreferences pref = getSharedPreferences("com.goncanapp.travels_preferences", Context.MODE_PRIVATE);
        ordenacion = Integer.parseInt(pref.getString("ordenacion", "1"));
        viajesPasados = pref.getBoolean("viajesSiguientes", true);

        //Rellenamos listview
        rellenarListaViajes(ordenacion,viajesPasados, null);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapter, View view, int position, long arg) {
                int idCursor = items.get(position).getID();
                Intent i = new Intent(MainActivity.this, Viaje.class);
                i.putExtra("id_viaje", idCursor);
                startActivity(i);
            }
        });
    }

    public void rellenarListaViajes(int ordenacion,boolean siguienteViajes, String search) {
        listView = (ListView) findViewById(R.id.listaViajes);
        items = new ArrayList<>();
        Cursor cursor = viajesBD.obtenerViajes(ordenacion);
        while (cursor.moveToNext()) {
            if(viajesPasados){
                //Setear imagen
                String tipo  = cursor.getString(3);
                if(!(search==null)){
                    if(cursor.getString(1).toLowerCase().contains(search.toLowerCase())){
                        if(tipo.equals("Vacaciones")){
                            items.add(new ElementoLista(R.drawable.vacaciones, cursor.getString(1), Integer.parseInt(cursor.getString(0))));
                        }else if(tipo.equals("Senderismo")) {
                            items.add(new ElementoLista(R.drawable.senderismo, cursor.getString(1), Integer.parseInt(cursor.getString(0))));
                        }else if(tipo.equals("Cultural")) {
                            items.add(new ElementoLista(R.drawable.cultural, cursor.getString(1), Integer.parseInt(cursor.getString(0))));
                        }else if(tipo.equals("Trabajo")) {
                            items.add(new ElementoLista(R.drawable.trabajo, cursor.getString(1), Integer.parseInt(cursor.getString(0))));
                        }else if(tipo.equals("Gastronómico")) {
                            items.add(new ElementoLista(R.drawable.gastronomia, cursor.getString(1), Integer.parseInt(cursor.getString(0))));
                        }
                    }
                }else{
                    if(tipo.equals("Vacaciones")){
                        items.add(new ElementoLista(R.drawable.vacaciones, cursor.getString(1), Integer.parseInt(cursor.getString(0))));
                    }else if(tipo.equals("Senderismo")) {
                        items.add(new ElementoLista(R.drawable.senderismo, cursor.getString(1), Integer.parseInt(cursor.getString(0))));
                    }else if(tipo.equals("Cultural")) {
                        items.add(new ElementoLista(R.drawable.cultural, cursor.getString(1), Integer.parseInt(cursor.getString(0))));
                    }else if(tipo.equals("Trabajo")) {
                        items.add(new ElementoLista(R.drawable.trabajo, cursor.getString(1), Integer.parseInt(cursor.getString(0))));
                    }else if(tipo.equals("Gastronómico")) {
                        items.add(new ElementoLista(R.drawable.gastronomia, cursor.getString(1), Integer.parseInt(cursor.getString(0))));
                    }
                }
            }else{
                if(util.ConvertToDate(cursor.getString(4).toString()).after(util.getFormatDate()) || util.ConvertToDate(cursor.getString(4).toString()).equals(util.getFormatDate())){
                    //Setear imagen
                    String tipo  = cursor.getString(3);
                    if(!(search==null)){
                        if(cursor.getString(1).toLowerCase().contains(search.toLowerCase())){
                            if(tipo.equals("Vacaciones")){
                                items.add(new ElementoLista(R.drawable.vacaciones, cursor.getString(1), Integer.parseInt(cursor.getString(0))));
                            }else if(tipo.equals("Senderismo")) {
                                items.add(new ElementoLista(R.drawable.senderismo, cursor.getString(1), Integer.parseInt(cursor.getString(0))));
                            }else if(tipo.equals("Cultural")) {
                                items.add(new ElementoLista(R.drawable.cultural, cursor.getString(1), Integer.parseInt(cursor.getString(0))));
                            }else if(tipo.equals("Trabajo")) {
                                items.add(new ElementoLista(R.drawable.trabajo, cursor.getString(1), Integer.parseInt(cursor.getString(0))));
                            }else if(tipo.equals("Gastronómico")) {
                                items.add(new ElementoLista(R.drawable.gastronomia, cursor.getString(1), Integer.parseInt(cursor.getString(0))));
                            }
                        }
                    }else{
                        if(tipo.equals("Vacaciones")){
                            items.add(new ElementoLista(R.drawable.vacaciones, cursor.getString(1), Integer.parseInt(cursor.getString(0))));
                        }else if(tipo.equals("Senderismo")) {
                            items.add(new ElementoLista(R.drawable.senderismo, cursor.getString(1), Integer.parseInt(cursor.getString(0))));
                        }else if(tipo.equals("Cultural")) {
                            items.add(new ElementoLista(R.drawable.cultural, cursor.getString(1), Integer.parseInt(cursor.getString(0))));
                        }else if(tipo.equals("Trabajo")) {
                            items.add(new ElementoLista(R.drawable.trabajo, cursor.getString(1), Integer.parseInt(cursor.getString(0))));
                        }else if(tipo.equals("Gastronómico")) {
                            items.add(new ElementoLista(R.drawable.gastronomia, cursor.getString(1), Integer.parseInt(cursor.getString(0))));
                        }
                    }
                }
            }
        }
        this.listView.setAdapter(new ElementoAdaptador(this, items));
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        mSearchAction = menu.findItem(R.id.accion_buscar);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.anadir_Viaje:
                Intent i = new Intent(this, AnadirViaje.class);
                i.putExtra("modificar", false);
                startActivity(i);
                return true;
            case R.id.configuracion:
                Intent i2= new Intent(this,Preferencias.class);
                startActivityForResult(i2, 1);
                return true;
            case R.id.acercaDe:
                Intent i3= new Intent(this,AcercaDe.class);
                startActivity(i3);
                return true;
            case R.id.accion_buscar:
                handleMenuSearch();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            SharedPreferences pref = getSharedPreferences("com.goncanapp.travels_preferences", Context.MODE_PRIVATE);
            String ordenacion = pref.getString("ordenacion", "1");
            viajesPasados = pref.getBoolean("viajesSiguientes", true);
            rellenarListaViajes(Integer.parseInt(ordenacion), viajesPasados, null);
        }
    }

    protected void handleMenuSearch() {
        action = getSupportActionBar(); //get the actionbar

        if (isSearchOpened) { //test if the search is open

            action.setDisplayShowCustomEnabled(false); //disable a custom view inside the actionbar
            action.setDisplayShowTitleEnabled(true); //show the title in the action bar

            //hides the keyboard
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(edtSeach.getWindowToken(), 0);

            //add the search icon in the action bar
            mSearchAction.setIcon(getResources().getDrawable(R.mipmap.ic_search));

            isSearchOpened = false;
        } else { //open the search entry

            action.setDisplayShowCustomEnabled(true); //enable it to display a
            // custom view in the action bar.
            action.setCustomView(R.layout.search_bar);//add the custom view
            action.setDisplayShowTitleEnabled(false); //hide the title

            edtSeach = (EditText) action.getCustomView().findViewById(R.id.edtSearch);
            edtSeach.addTextChangedListener(new TextWatcher() {
                public void afterTextChanged(Editable s) {
                    rellenarListaViajes(ordenacion, viajesPasados, s.toString());
                }

                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                public void onTextChanged(CharSequence s, int start, int before, int count) {
                }
            });

            edtSeach.requestFocus();

            //open the keyboard focused in the edtSearch
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(edtSeach, InputMethodManager.SHOW_IMPLICIT);

            //add the close icon
            mSearchAction.setIcon(getResources().getDrawable(R.drawable.ic_close_search));

            isSearchOpened = true;
        }
    }
}
