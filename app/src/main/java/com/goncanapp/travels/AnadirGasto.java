package com.goncanapp.travels;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import java.util.Calendar;

/**
 * Created by Alberto on 06/09/2015.
 */
public class AnadirGasto extends AppCompatActivity implements OnClickListener{

    ImageButton btnFechagasto;
    EditText nombreGasto, importe, fechaGasto;
    private int mYear, mMonth, mDay;
    Button botonNuevoGasto;
    private Context contexto;
    Spinner tipoGasto;
    int id_viaje, id_gasto;
    boolean modificar;

    private static ViajesBD viajesBD;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.anadir_gasto);
        contexto = this;

        //Appbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.appbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        //Agragar titulo en ActionBar
        TextView titulo = (TextView) findViewById(R.id.titulo);
        titulo.setText(R.string.anadir_gasto);

        //Fecha
        fechaGasto = (EditText) findViewById(R.id.fechaGasto);
        btnFechagasto = (ImageButton) findViewById(R.id.botonFechaInicio);
        btnFechagasto.setOnClickListener(this);

        nombreGasto = (EditText) findViewById(R.id.nombreGasto);
        importe = (EditText) findViewById(R.id.importe);
        tipoGasto =(Spinner) findViewById(R.id.tipoGasto);
        botonNuevoGasto = (Button) findViewById(R.id.aceptarGasto);

        viajesBD = new ViajesBD(contexto);

        Bundle extras = getIntent().getExtras();
        id_viaje = extras.getInt("id_viaje");

        //Obtener valores del viaje de la base de datos
        modificar = extras.getBoolean("modificar");
        if(modificar){
            id_gasto  = extras.getInt("id_gasto");
            Cursor cursor = viajesBD.obtenerGasto(id_gasto);
            cursor.moveToNext();
            nombreGasto.setText(cursor.getString(1).toString());
            importe.setText(cursor.getString(2).toString());
            fechaGasto.setText(cursor.getString(4).toString());

            String[] tipos = getResources().getStringArray(R.array.tipoGasto);
            for(int i=0; i<tipos.length; i++){
                //noinspection RedundantStringToString
                if(tipos[i].equals(cursor.getString(3).toString())){
                    tipoGasto.setSelection(i);
                }
            }

        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_anadir_gastos, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == R.id.configuracion) {
            Intent i = new Intent(this, Preferencias.class);
            startActivity(i);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.botonFechaInicio:
                // Process to get Current Date
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dpd = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                // Setear valor en editText
                                fechaGasto.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                            }
                        }, mYear, mMonth, mDay);
                dpd.show();
                break;
        }
    }

    public void aceptarGasto(View view) {
        if (nombreGasto.getText().toString().matches("") || importe.getText().toString().matches("") || tipoGasto.getSelectedItem().toString().matches("") ||
                fechaGasto.getText().toString().matches("")) {
            Toast toast3 = new Toast(getApplicationContext());

            LayoutInflater inflater = getLayoutInflater();
            View layout = inflater.inflate(R.layout.toast_layout, (ViewGroup) findViewById(R.id.lytLayout));

            TextView txtMsg = (TextView) layout.findViewById(R.id.txtMensaje);
            txtMsg.setText(R.string.errorForm);

            toast3.setDuration(Toast.LENGTH_SHORT);
            toast3.setView(layout);
            toast3.show();
        //}else if (Double.parseDouble(0,9)) {
        }else{
            if(modificar){
                viajesBD.modificarGasto(nombreGasto.getText().toString(), importe.getText().toString(), tipoGasto.getSelectedItem().toString(),
                        fechaGasto.getText().toString(), id_viaje, id_gasto);
                Intent i = new Intent(this, Gastos.class);
                i.putExtra("id_viaje", id_viaje);
                startActivity(i);
                finish();
            }else{
                viajesBD.guardarGasto(nombreGasto.getText().toString(), importe.getText().toString(), tipoGasto.getSelectedItem().toString(),
                        fechaGasto.getText().toString(), id_viaje);
                Intent i = new Intent(this, Gastos.class);
                i.putExtra("id_viaje", id_viaje);
                startActivity(i);
                finish();
            }
        }
    }

    public void onBackPressed(){
        Intent i = new Intent(this, Gastos.class);
        i.putExtra("id_viaje", id_viaje);
        startActivity(i);
    }

}
